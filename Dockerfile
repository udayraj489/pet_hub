From node:21

WORKDIR /app

COPY . /app 

RUN npm install react-scripts

RUN npm install

EXPOSE 3000

CMD [ "npm" ,"start" ]
